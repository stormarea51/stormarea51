﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class ScoreTracker : MonoBehaviour {

    //Author: Brian Leung

    // Tracks score and keeps HighScore
    // Win and Lose Condition activates here

    public float score;

    public Text highScore;
    public Text scoreText;


    public GameObject winPanel;
    public GameObject losePanel;

	void Start ()
    {
        highScore.text = PlayerPrefs.GetFloat("HighScore", 0).ToString("0");
        winPanel.SetActive(false);
        losePanel.SetActive(false);
    }
	
	
	void Update ()
    {
        scoreText.text = score.ToString("0");

        if(score >= 100)
        {
            score += 5 * Time.deltaTime;
        }else if(score < 100)
        {
            score += 2 * Time.deltaTime;
        }
        /*if (energy.energy <= 0)
        {
            if (score > PlayerPrefs.GetFloat("HighScore", 0))
            {
                UpdateScore();
            }
                
            if(score < PlayerPrefs.GetFloat("HighScore", 0))
            {
                losePanel.SetActive(true);
            }
        }*/
        
    }

    public void ScoreCount(int scores)
    {
        score += scores;
    }

    void UpdateScore()
    {
        if(score > PlayerPrefs.GetFloat("HighScore",0))
        {
            PlayerPrefs.SetFloat("HighScore", score);
            winPanel.SetActive(true);
            highScore.text = score.ToString("0");
        }
    }
}
