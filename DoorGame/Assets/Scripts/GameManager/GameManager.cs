﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    private Health playerHealth;
    public GameObject losePanel;

    void Start()
    {
        playerHealth = GameObject.Find("Player").GetComponent<Health>();
    }

    // Update is called once per frame
    void Update()
    {
        PlayerDeath();
    }
    
    void PlayerDeath()
    {
        if(playerHealth.health < 1)
        {
            losePanel.SetActive(true);
        }
    }
}
