﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AltTimeTrigger : MonoBehaviour
{
    [SerializeField] float addSlower = 2.0f;

	void OnTriggerEnter(Collider player)
    {
        if (player.gameObject.CompareTag("Player"))
        {
            Debug.Log("hit");

            TimeManager.instance.slowTime();
            TimeManager.instance.slowedTimer += addSlower;
            gameObject.SetActive(false);
            }
        }
}
