﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Author: Nate Hales
/// This is the keeper of time scale if you need to change or alter anything call one of these functions to do so.
/// </summary>
public class TimeManager : MonoBehaviour
{
    #region Singleton
    private static TimeManager _instance;
    public static TimeManager instance
    {
        get
        {
            if (_instance == null)
            {
                GameObject tM = new GameObject("TimeManager");
               _instance = tM.AddComponent<TimeManager>();
               
            }
            return _instance;
        }
    }
    #endregion


    public bool timeSlowed, isPaused;
    public GameObject pauseMenu;
    public float slowedTimer = 2.0f, timereScaler = .40f, holdTime = 0f;

    void FixedUpdate()
    {
        if (timeSlowed)
        {
            if (0.0f >= slowedTimer && Time.timeScale != 1.0f)
            {
                Time.timeScale = 1.0f;
                slowedTimer = 0.0f;
            }
            if (0.0f <= slowedTimer && Time.timeScale != 1.0f && !isPaused)
            {
                slowedTimer--;
            }
        }
    }

    public void slowTime()
    {
        timeSlowed = true;
        Time.timeScale = timereScaler;
        return;
    }

    public void resumeTime()
    {
        Time.timeScale = holdTime;
        isPaused = false;
        //pauseMenu.SetActive(false);
    }

    public void stopTime()
    {
        holdTime = Time.timeScale;
        Time.timeScale = 0.0f;
        isPaused = true;
        //pauseMenu.SetActive(true);
    }

    //TODO: Have the game manager resume time in theses cases.
    private void OnDestroy()
    {
        resumeTime();
    }
}
