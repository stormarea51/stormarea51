﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneChanger : MonoBehaviour {
	//Author; Brian Leung

    //Changes Scene
	public string scene;

	public void SceneChange()
	{
		//Changes to the scene you want to go to
		SceneManager.LoadScene (scene);
	}

	public void Quit()
	{
		//Closes the application
		Application.Quit ();
	}

}
