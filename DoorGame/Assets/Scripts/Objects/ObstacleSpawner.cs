﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleSpawner : MonoBehaviour
{
    public List<Transform> obsSpawners;
    public GameObject obstacle;

    Health playerHealth;
    public float timeCounter;
    public float spawnRate;

    void Awake()
    {
        foreach(Transform child in transform)
        {
            obsSpawners.Add(child);
        }
    }
    void Start()
    {
        timeCounter = spawnRate;
        playerHealth = GameObject.Find("Player").GetComponent<Health>();
    }
    void Update()
    {
        timeCounter -= 1 * Time.deltaTime;
        if (timeCounter <= 0)
        {
            int randNum = Random.Range(0, obsSpawners.Count);
            Instantiate(obstacle, obsSpawners[randNum].position, Quaternion.identity);
            timeCounter = spawnRate;
        }
    }
}
