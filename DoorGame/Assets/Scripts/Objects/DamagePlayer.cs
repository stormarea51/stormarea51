﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamagePlayer : MonoBehaviour
{
    public Health playerHealth;
    void Start()
    {
        playerHealth = GameObject.Find("Player").GetComponent<Health>();
    }
    
    void OnTriggerEnter(Collider player)
    {
        if(player.gameObject.tag == "Player")
        {
            playerHealth.health -= 1;
        }
    }
}
