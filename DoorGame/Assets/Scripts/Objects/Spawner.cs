﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    //Author: Brian Leung

    // Spawns the game

    public GameObject platform;
    public GameObject stream;

    private Health playerHealth;

    void Start ()
    {
        playerHealth = GameObject.Find("Player").GetComponent<Health>();
        InvokeRepeating("SpawnPlatform", 5.0f, 3);
	}
	
	
	void Update ()
    {
        if(playerHealth.health < 1)
        {
            CancelInvoke("SpawnPlatform");
        }
	}

    void SpawnPlatform()
    {
        Instantiate(platform, (new Vector3(0, 0, this.transform.position.z + 25)), Quaternion.identity);
        Destroy();
    }

    void Destroy()
    {
        Destroy(this.gameObject);
    }
   
}
