﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectMove : MonoBehaviour {
    //Author: Brian Leung

    //Moves the Objects the Player has to dodge

    Health playerHealth;
	public float speed;

	void Start () 
	{
        playerHealth = GameObject.Find("Player").GetComponent<Health>();

    }
	

	void Update () 
	{
        //moves the object towards the player
        transform.Translate(0, 0, speed * Time.deltaTime);
        SlowDownOnDeath();


    }

    void SlowDownOnDeath()
    {
        if (playerHealth.health < 1 && speed < 0)
        {
            speed += 2 * Time.deltaTime;
        }
    }
}
