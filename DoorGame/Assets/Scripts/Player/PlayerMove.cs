﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMove : MonoBehaviour
{
    public Swipe swipeControls;
    public GameObject[] playerSpots;
    public Transform player;
    public float jumpForce;

    private int spotNumber;
    private Rigidbody rb;
    private Animator anim;

    void Start()
    {
        rb = GetComponent<Rigidbody>();
        //anim = GetComponent<Animator>();
        spotNumber = 1;
        player.position = playerSpots[1].transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        Movement();
        player.position = Vector3.Lerp(player.position, new Vector3(playerSpots[spotNumber].transform.position.x, player.position.y, player.position.z), 10 * Time.deltaTime);
    }

    void Movement()
    {
        //anim.SetBool("Run", true);
        if (spotNumber <= 1 && swipeControls.SwipeRight)
        {
            spotNumber++;
        }
        else if (spotNumber >= 1 && swipeControls.SwipeLeft)
        {
            spotNumber--;
        }

        if (swipeControls.SwipeUp && IsGrounded())
        {
            Jump();
        }
        if(swipeControls.SwipeDown && IsGrounded())
        {
            Slide();
        }
    }

    void Jump()
    {
        //anim.SetTrigger("Jump");
        rb.velocity = Vector3.up * Mathf.Pow(jumpForce, 2) * Time.deltaTime;
    }
    void Slide()
    {
        //anim.SetTrigger("Slide");
        //play Animation
        //Collider Shrinks
    }
    bool IsGrounded()
    {
        return Physics.Raycast(transform.position, -Vector3.up, GetComponent<Collider>().bounds.size.y/2 + 0.1f);
    }

}
